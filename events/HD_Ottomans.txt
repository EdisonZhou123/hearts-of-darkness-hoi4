﻿###########################
# Darkest Hour Election Events : Australia
###########################
add_namespace = HD_Young_Turks_Revolution
#########################################################################
#  Macedonian Reservists Refuse Call to Service
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.1
	title = HD_Young_Turks_Revolution.1.t
	desc = HD_Young_Turks_Revolution.1.d
	picture = GFX_report_Macedonia
	
	fire_only_once = yes	
	trigger = {
		original_tag = OTT
		date > 1908.2.7
		date < 1908.3.1
	}
	
	# Dispatch a commission immediately
	option = {
		name = HD_Young_Turks_Revolution.1.A
		ai_chance = { 
			factor = 1
		}
		add_manpower = -2000
		country_event = {
			id = HD_Young_Turks_Revolution.2
			days = 30
		}
		hidden_effect = {
			country_event = {
				id = HD_Young_Turks_Revolution.4
				days = 121
			}
			set_global_flag = Young_turks_revolution
		}
	}
}

#########################################################################
#  Report from the commission
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.2
	title = HD_Young_Turks_Revolution.2.t
	desc = HD_Young_Turks_Revolution.2.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes	
	is_triggered_only = yes
	
	# Infiltrate the garrison with spies, at least.
	option = {
		name = HD_Young_Turks_Revolution.2.A
		ai_chance = { 
			factor = 1
		}
	}
}

#########################################################################
#  Circulation of a "Committee of Union and Progress" Manifesto
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.3
	title = HD_Young_Turks_Revolution.3.t
	desc = HD_Young_Turks_Revolution.3.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes
	trigger = {
		original_tag = OTT
		date > 1908.5.1
		date < 1908.6.1
	}
	
	# Worrying
	option = {
		name = HD_Young_Turks_Revolution.3.A
		ai_chance = { 
			factor = 1
		}
		add_stability = -0.01
	}
}

#########################################################################
#  Mutinous Soldiers Demand Payment
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.4
	title = HD_Young_Turks_Revolution.4.t
	desc = HD_Young_Turks_Revolution.4.d
	picture = GFX_report_Macedonia
	
	fire_only_once = yes	
	is_triggered_only = yes
	
	# Refuse to promote individuals who hold these attitudes.
	option = {
		name = HD_Young_Turks_Revolution.4.A
		ai_chance = { 
			factor = 1
		}
		country_event = {
			id = HD_Young_Turks_Revolution.6
			days = 26
		}
	}
}

#########################################################################
#  Anglo-Russian Meeting in Reval
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.5
	title = HD_Young_Turks_Revolution.5.t
	desc = HD_Young_Turks_Revolution.5.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes	
	trigger = {
		original_tag = OTT
		date > 1908.6.9
		date < 1908.7.1
	}
	
	# Neither England nor Russia can answer the Eastern Question for themselves.
	option = {
		name = HD_Young_Turks_Revolution.5.A
		ai_chance = { 
			factor = 1
		}
		add_stability = -0.02
	}
}

#########################################################################
#  Rebellious Troops Raised in Macedonia
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.6
	title = HD_Young_Turks_Revolution.6.t
	desc = HD_Young_Turks_Revolution.6.d
	picture = GFX_report_revolt_Macedonia
	
	fire_only_once = yes	
	is_triggered_only = yes
	
	# The soldiers are disorganised, it will surely come to nothing.
	option = {
		name = HD_Young_Turks_Revolution.6.A
		ai_chance = { 
			factor = 1
		}
		add_stability = -0.03
		hidden_effect = {
			country_event = {
				id = HD_Young_Turks_Revolution.7
				days = 3
			}
			country_event = {
				id = HD_Young_Turks_Revolution.8
				days = 10
			}
		}
	}
}

#########################################################################
#  Assassination of Shemsi Pasha
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.7
	title = HD_Young_Turks_Revolution.7.t
	desc = HD_Young_Turks_Revolution.7.d
	picture = GFX_report_Pasha
	
	fire_only_once = yes	
	is_triggered_only = yes
	
	# This is more serious than first thought
	option = {
		name = HD_Young_Turks_Revolution.7.A
		ai_chance = { 
			factor = 1
		}
		add_stability = -0.05
	}
}

#########################################################################
#  Mass-promotion of Junior Officers
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.8
	title = HD_Young_Turks_Revolution.8.t
	desc = HD_Young_Turks_Revolution.8.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes	
	is_triggered_only = yes
	
	# This is more serious than first thought
	option = {
		name = HD_Young_Turks_Revolution.8.A
		ai_chance = { 
			factor = 1
		}
		add_timed_idea = {
			idea = Mass_promotion_of_Junior_Officers
			days = 200
		}
		hidden_effect = {
			country_event = {
				id = HD_Young_Turks_Revolution.9
				days = 7
			}
			country_event = {
				id = HD_Young_Turks_Revolution.10
				days = 9
			}
		}
	}
}

#########################################################################
#  Anatolian Reinforcements Defect
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.9
	title = HD_Young_Turks_Revolution.9.t
	desc = HD_Young_Turks_Revolution.9.d
	picture = GFX_report_revolt_Macedonia
	
	fire_only_once = yes	
	is_triggered_only = yes
	
	# This is getting more and more desesperate.
	option = {
		name = HD_Young_Turks_Revolution.9.A
		ai_chance = { 
			factor = 1
		}
		add_stability = -0.10
	}
}

#########################################################################
#  Proclamation of a Constitution in Monastir
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.10
	title = HD_Young_Turks_Revolution.10.t
	desc = HD_Young_Turks_Revolution.10.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = { 
			country_event = {
				id = HD_Young_Turks_Revolution.16
				days = 147
			}
		}
	}
	
	# We have no choice but to give in to their demand.
	option = {
		name = HD_Young_Turks_Revolution.10.A
		ai_chance = { 
			factor = 50
		}
		add_stability = -0.10
		country_event = {
			id = HD_Young_Turks_Revolution.11
			days = 1
		}
	}
	# Send an army to meet them in their march.
	option = {
		name = HD_Young_Turks_Revolution.10.B
		ai_chance = { 
			factor = 50
			modifier = {
				is_historical_focus_on = yes
				factor = 0
			}
		}
		add_stability = -0.10
		hidden_effect = {
			country_event = {
				id = HD_Young_Turks_Revolution.13
				days = 12
			}
		}
	}
}

#########################################################################
#  Sultan Abdul Hamid II Restores the Constitution
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.11
	title = HD_Young_Turks_Revolution.11.t
	desc = HD_Young_Turks_Revolution.11.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes	
	is_triggered_only = yes
	
	# The rejuvenation of the Ottoman Empire begins today.
	option = {
		name = HD_Young_Turks_Revolution.11.A
		ai_chance = { 
			factor = 1
		}
		add_stability = 0.25
		swap_ideas = {
			remove_idea = OTT_HoG_Avlonyali_Mehmed_Ferid
			add_idea = OTT_HoG_Mehmed_Said
		}
		hidden_effect = {
			country_event = {
				id = HD_Young_Turks_Revolution.17
				days = 15
			}
			news_event = {
				id = HD_Young_Turks_Revolution.12
				days = 3
			}
		}
	}
}
#########################################################################
#  Sultan Abdul Hamid II Restores the Constitution NEWS EVENT
#########################################################################
news_event = {
	id = HD_Young_Turks_Revolution.12
	title = HD_Young_Turks_Revolution.12.t
	desc = HD_Young_Turks_Revolution.12.d
	picture = GFX_news_ENG_London_Fire
	show_major = {
		is_european_great_powers = yes
		is_interesting_middle_east_powers = yes
		is_interesting_balkan_power = yes
	}
	is_triggered_only = yes
	
	# The rejuvenation of the Ottoman Empire begins today.
	option = {
		name = HD_Young_Turks_Revolution.12.A
		ai_chance = { 
			factor = 1
		}
	}
}

#########################################################################
#  Rebel Troops Enter the Capital
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.13
	title = HD_Young_Turks_Revolution.13.t
	desc = HD_Young_Turks_Revolution.13.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes	
	is_triggered_only = yes
	
	# Long Live Mehmed V!
	option = {
		name = HD_Young_Turks_Revolution.13.A
		ai_chance = { 
			factor = 1
		}
		add_stability = 0.2
		swap_ideas = {
			remove_idea = OTT_HoG_Avlonyali_Mehmed_Ferid
			add_idea = OTT_HoG_Ismail_Enver
		}
		create_country_leader = {
			name = "Mehmed V"
			desc = "Mehmed_V_desc"
			picture = "P_A_Mehmed_V.tga"
			expire = "1918.7.3"
			ideology = monarchism
			traits = { ideology_A POSITION_Sultan SUBIDEOLOGY_Theocracy }
		}
		hidden_effect = {
			news_event = {
				id = HD_Young_Turks_Revolution.14
				days = 1
			}
		}
	}
}
#########################################################################
#  Rebel Troops Enter the Ottoman Capital NEWS EVENT
#########################################################################
news_event = {
	id = HD_Young_Turks_Revolution.14
	title = HD_Young_Turks_Revolution.14.t
	desc = HD_Young_Turks_Revolution.14.d
	picture = GFX_news_ENG_London_Fire
	show_major = {
		is_european_great_powers = yes
		is_interesting_middle_east_powers = yes
		is_interesting_balkan_power = yes
	}
	is_triggered_only = yes
	
	# Long Live Mehmed V!
	option = {
		name = HD_Young_Turks_Revolution.14.A
		ai_chance = { 
			factor = 1
		}
	}
}

#########################################################################
#  Mass Releases of Prisoners
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.15
	title = HD_Young_Turks_Revolution.15.t
	desc = HD_Young_Turks_Revolution.15.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes	
	is_triggered_only = yes
	
	trigger = {
		original_tag = OTT
		date > 1908.8.5
		date < 1908.7.1
	}
	
	# You are meant to release political prisoners, not common criminals!
	option = {
		name = HD_Young_Turks_Revolution.15.A
		ai_chance = { 
			factor = 1
		}
		add_stability = -0.03
		add_political_power = -20
	}
}

#########################################################################
#  CUP Wins the Most Seats in the Ottoman General Election
#########################################################################
news_event = { 
	id = HD_Young_Turks_Revolution.16
	title = HD_Young_Turks_Revolution.16.t
	desc = HD_Young_Turks_Revolution.16.d
	picture = GFX_news_ENG_London_Fire
	
	fire_only_once = yes	
	is_triggered_only = yes
	
	
	# Quite Predictably.
	option = {
		name = HD_Young_Turks_Revolution.16.A
		ai_chance = { 
			factor = 1
		}
	}
}

#########################################################################
#  Young Turks Force the Replacement of Mehmed Said Pasha
#########################################################################
country_event = { 
	id = HD_Young_Turks_Revolution.17
	title = HD_Young_Turks_Revolution.17.t
	desc = HD_Young_Turks_Revolution.17.d
	picture = GFX_DH_Generic_Elections
	
	fire_only_once = yes	
	is_triggered_only = yes
	
	# Who is the Sultan here?
	option = {
		name = HD_Young_Turks_Revolution.17.A
		ai_chance = { 
			factor = 1
		}
		swap_ideas = {
			remove_idea = OTT_HoG_Mehmed_Said
			add_idea = OTT_HoG_Kibrisli_Mehmed_Kamil
		}
	}
}