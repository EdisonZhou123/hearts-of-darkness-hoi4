ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Zhang Zuolin
		FNT_HoG_Zhang_Zuolin = {
			picture = Generic_Portrait
			allowed = { tag = FNT }
			available = {
				date > 1914.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Zhang_Zuolin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_General }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Wang Yongjiang
		FNT_FM_Wang_Yongjiang = {
			picture = Generic_Portrait
			allowed = { tag = FNT }
			available = {
				date > 1914.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wang_Yongjiang_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Biased_Intellectual }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Zhang Zongchang
		FNT_MoS_Zhang_Zongchang = {
			picture = Generic_Portrait
			allowed = { tag = FNT }
			available = {
				date > 1914.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Zhang_Zongchang_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Prince_Of_Terror }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Wang Yongjiang
		FNT_AM_Wang_Yongjiang = {
			picture = Generic_Portrait
			allowed = { tag = FNT }
			available = {
				date > 1914.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wang_Yongjiang_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Keynesian_Economy }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Guo Songling
		FNT_HoI_Guo_Songling = {
			picture = Generic_Portrait
			allowed = { tag = FNT }
			available = {
				date > 1914.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Guo_Songling_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Logistics_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Zhang Zuolin
		FNT_CoStaff_Zhang_Zuolin = {
			picture = Generic_Portrait
			allowed = { tag = FNT }
			available = {
				date > 1914.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Zhang_Zuolin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Zhang Jinghui
		FNT_CoArmy_Zhang_Jinghui = {
			picture = Generic_Portrait
			allowed = { tag = FNT }
			available = {
				date > 1914.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Zhang_Jinghui_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Decisive_Battle_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Under Army Command
		FNT_CoNavy_Under_Army_Command = {
			picture = Generic_Portrait
			allowed = { tag = FNT }
			available = {
				date > 1914.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Under_Army_Command_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Decisive_Naval_Battle_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Zhang Xueliang
		FNT_CoAir_Zhang_Xueliang = {
			picture = Generic_Portrait
			allowed = { tag = FNT }
			available = {
				date > 1914.1.1
				date < 1964.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Zhang_Xueliang_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Army_Aviation_Doctrine }
		}
	}
}
