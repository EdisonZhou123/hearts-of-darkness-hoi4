ideas = {
#################################################
### Head of Government
#################################################
	Head_of_Government = {
	# Cao Kun
		ZHI_HoG_Cao_Kun = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1885.1.1
				date < 1925.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Cao_Kun_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Political_Protege }
		}
	# Gao Lingwei
		ZHI_HoG_Gao_Lingwei = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1910.1.1
				date < 1926.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Gao_Lingwei_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Political_Protege }
		}
	# Du Xigui
		ZHI_HoG_Du_Xigui = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1911.1.1
				date < 1927.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Du_Xigui_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoG_Old_Admiral }
		}
}
#################################################
### Foreign Minister
#################################################
	Foreign_Minister = {
	# Feng Guozhang
		ZHI_FM_Feng_Guozhang = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1903.1.1
				date < 1919.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Feng_Guozhang_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_General_Staffer }
		}
	# Gao Lingwei
		ZHI_FM_Gao_Lingwei = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1910.1.1
				date < 1926.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Gao_Lingwei_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A FM_Biased_Intellectual }
		}
}
#################################################
### Minister of Security
#################################################
	Minister_of_Security = {
	# Gao Lingwei
		ZHI_MoS_Gao_Lingwei = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1910.1.1
				date < 1926.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Gao_Lingwei_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Compassionate_Gentleman }
		}
	# Wang Tan
		ZHI_MoS_Wang_Tan = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1911.1.1
				date < 1924.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wang_Tan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Crime_Fighter }
		}
	# Deng Xihou
		ZHI_MoS_Deng_Xihou = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1917.1.1
				date < 1926.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Deng_Xihou_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A MoS_Prince_Of_Terror }
		}
}
#################################################
### Armaments Minister
#################################################
	Armaments_Minister = {
	# Liu Enyuan
		ZHI_AM_Liu_Enyuan = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1895.1.1
				date < 1924.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Liu_Enyuan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Military_Entrepreneur }
		}
	# Wu Yulin
		ZHI_AM_Wu_Yulin = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1895.1.1
				date < 1925.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wu_Yulin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Battle_Fleet_Proponent }
		}
	# Gao Lingwei
		ZHI_AM_Gao_Lingwei = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1910.1.1
				date < 1926.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Gao_Lingwei_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Resource_Industrialist }
		}
	# Gao Enhong
		ZHI_AM_Gao_Enhong = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1912.1.1
				date < 1925.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Gao_Enhong_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Resource_Industrialist }
		}
	# Zhang Yinghua
		ZHI_AM_Zhang_Yinghua = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1918.1.1
				date < 1926.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Zhang_Yinghua_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A AM_Administrative_Genius }
		}
}
#################################################
### Head of Intelligence
#################################################
	Head_of_Intelligence = {
	# Cao Kun
		ZHI_HoI_Cao_Kun = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1885.1.1
				date < 1925.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Cao_Kun_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Political_Specialist }
		}
	# Wu Peifu
		ZHI_HoI_Wu_Peifu = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1902.1.1
				date < 1927.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wu_Peifu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A HoI_Logistics_Specialist }
		}
}
#################################################
### Chief of Staff
#################################################
	Chief_of_Staff = {
	# Wu Peifu
		ZHI_CoStaff_Wu_Peifu = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1902.1.1
				date < 1927.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wu_Peifu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Manoeuvre }
		}
	# Zhang Shaozeng
		ZHI_CoStaff_Zhang_Shaozeng = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1903.1.1
				date < 1929.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Zhang_Shaozeng_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Psychology }
		}
	# Wang Tan
		ZHI_CoStaff_Wang_Tan = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1911.1.1
				date < 1924.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wang_Tan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Mass_Combat }
		}
	# Lu Jin
		ZHI_CoStaff_Lu_Jin = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1912.1.1
				date < 1925.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Lu_Jin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
}
#################################################
### Chief of Army
#################################################
	Chief_of_Army = {
	# Sun Chuanfang
		ZHI_CoArmy_Sun_Chuanfang = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1901.1.1
				date < 1931.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Sun_Chuanfang_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Decisive_Battle_Doctrine }
		}
	# Wu Peifu
		ZHI_CoArmy_Wu_Peifu = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1902.1.1
				date < 1927.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wu_Peifu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoStaff_School_Of_Defence }
		}
	# Zhang Shaozeng
		ZHI_CoArmy_Zhang_Shaozeng = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1903.1.1
				date < 1929.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Zhang_Shaozeng_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Decisive_Battle_Doctrine }
		}
	# Wang Tan
		ZHI_CoArmy_Wang_Tan = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1911.1.1
				date < 1924.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wang_Tan_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Guns_And_Butter_Doctrine }
		}
	# Lu Jin
		ZHI_CoArmy_Lu_Jin = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1912.1.1
				date < 1925.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Lu_Jin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoArmy_Static_Defence_Doctrine }
		}
}
#################################################
### Chief of Navy
#################################################
	Chief_of_Navy = {
	# Li Dingxin
		ZHI_CoNavy_Li_Dingxin = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1886.1.1
				date < 1930.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Li_Dingxin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Decisive_Naval_Battle_Doctrine }
		}
	# Wu Yulin
		ZHI_CoNavy_Wu_Yulin = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1895.1.1
				date < 1925.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wu_Yulin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Open_Seas_Doctrine }
		}
	# Du Xigui
		ZHI_CoNavy_Du_Xigui = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1911.1.1
				date < 1927.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Du_Xigui_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoNavy_Decisive_Naval_Battle_Doctrine }
		}
}
#################################################
### Chief of Airforce
#################################################
	Chief_of_Airforce = {
	# Wu Peifu
		ZHI_CoAir_Wu_Peifu = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1902.1.1
				date < 1927.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wu_Peifu_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Army_Aviation_Doctrine }
		}
	# Wang Chengbin
		ZHI_CoAir_Wang_Chengbin = {
			picture = Generic_Portrait
			allowed = { tag = ZHI }
			available = {
				date > 1913.1.1
				date < 1925.1.1
				A_Minister_Allowed = yes
				NOT = { has_country_flag = Wang_Chengbin_unavailable }
			}
			cost = 150
			removal_cost = 10
			traits = { ideology_A CoAir_Carpet_Bombing_Doctrine }
		}
	}
}
