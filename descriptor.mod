version="0.1"
supported_version="1.9.*"
name="Hearts of Darkness - Dev Build"
path="mod/hearts-of-darkness-hoi4/"
dependencies= {
   "Darkest Hour"
}
tags={
	"Alternative History"
	"Gameplay"
	"Events"
	"Map"
}
replace_path="common/bookmarks"
replace_path="history/states"
replace_path="map/strategicregions"