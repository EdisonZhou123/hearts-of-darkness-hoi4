
state={
	id=198
	name="STATE_198"

	history={
		owner = RUS
		buildings = {
			infrastructure = 5
			air_base = 3

		}
		add_core_of = RUS
		1918.11.11 = {
			owner = UKR
			controller = UKR
			add_core_of = UKR
			remove_core_of = RUS
		}

	}

	provinces={
		476 3430 6429 6455 9423 9435 9576 
	}
	manpower=1893391
	buildings_max_level_factor=1.000
	state_category=large_town
}
