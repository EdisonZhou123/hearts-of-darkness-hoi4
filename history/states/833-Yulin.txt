state={
	id=833
	name="STATE_833"

	history={
		1900.1.1 = {
			owner = QIN
			add_core_of = QIN
		}
		1914.1.1 = {
			owner = ROC
			remove_core_of = QIN
			add_core_of = ROC
		}
		1918.1.1 = {
			owner = YUL
			add_core_of = YUL
		}

		victory_points = {
			7659 1 
		}
		buildings = {
			infrastructure = 2
		}
	}

	provinces={
		1458 7659 7314 10880
	}
	manpower=1000000
	buildings_max_level_factor=1.000
	state_category=city
}
