
state={
	id=454
	name="STATE_454"

	history={
		owner = OTT
		1918.11.11 = {
			owner = ENG
			controller = ENG
			remove_core_of = OTT

		}
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			air_base = 2
			4206 = {
				naval_base = 3

			}
		}
		victory_points = {
			1201 5 
		}
		victory_points = {
			1065 1 
		}
		victory_points = {
			13214 10 
		}
		add_core_of = OTT

	}

	provinces={
		1065 1201 4206 7107 10104 13214 
	}
	manpower=531142
	buildings_max_level_factor=1.000
	state_category=rural
}
