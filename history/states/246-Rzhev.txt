
state={
	id=246
	name="STATE_246"
	resources={
		steel=24.000
	}

	history={
		owner = RUS
		1918.11.11 = {
			owner = SOV
			controller = SOV
			add_core_of = SOV

		}
		victory_points = {
			11349 1 
		}
		buildings = {
			infrastructure = 5

		}
		add_core_of = RUS

	}

	provinces={
		8 110 187 226 248 287 333 354 2999 3101 3215 3235 3279 3321 3344 3362 6086 6154 6225 6264 6273 6293 6303 6367 6387 6400 9111 9212 9242 9265 9287 9333 9343 9399 11191 11224 11234 11315 11326 11349 11384 
	}
	manpower=1025458
	buildings_max_level_factor=1.000
	state_category=rural
}
