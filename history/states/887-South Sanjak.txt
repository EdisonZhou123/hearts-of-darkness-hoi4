
state={
	id=887
	name="STATE_887"
	manpower = 24927
	
	state_category = rural

	history={
		owner = AUS
		buildings = {
			infrastructure = 1
		}
		add_core_of = OTT
		add_core_of = MNT
		1909.1.1 = {
			owner = OTT
		}
		1914.1.1 = {
			owner = MNT
		}
	}

	provinces={
		6913 13249
	}
}
