state={
	id=80
	name="STATE_80"
	manpower = 474600
	
	state_category = rural
	
	history=
	{
		owner = AUS
		1918.11.11 = {
			owner = ROM
			controller = ROM
			remove_core_of = AUS
		}
		buildings = {
			infrastructure = 6
			arms_factory = 1
		}
		add_core_of = AUS
		add_core_of = ROM
	}
	provinces={
		577 9548
	}
}
