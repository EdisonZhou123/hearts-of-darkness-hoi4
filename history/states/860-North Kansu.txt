state={
	id=860
	name="STATE_860"
	
	history={
		1900.1.1 = {
			owner = QIN
			add_core_of = QIN
		}
		1914.1.1 = {
			owner = ROC
			remove_core_of = QIN
			add_core_of = ROC
		}
		1918.1.1 = {
			owner = NXM
			add_core_of = NXM
		}
		
		buildings = {
			infrastructure = 1
		}


	}
	
	provinces={
		1778 2028 7971 12596 12820 
	}
	manpower=700000
	buildings_max_level_factor=1.000
	state_category=rural
}
