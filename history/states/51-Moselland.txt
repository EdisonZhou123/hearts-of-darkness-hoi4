
state={
	id=51
	name="STATE_51"
	resources={
		steel=16
	}

	history={
		owner = GER
		victory_points = {
			587 5 
		}
		buildings = {
			infrastructure = 8
			arms_factory = 2

		}
		add_core_of = GER

	}

	provinces={
		587 3444 3547 9522 
	}
	manpower=2560310
	buildings_max_level_factor=1.000
	state_category=large_town
}
