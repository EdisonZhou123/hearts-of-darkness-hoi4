
state={
	id=63
	name="STATE_63"
	resources={
		steel=36.000
		aluminium=12.000
	}

	history={
		owner = GER
		buildings = {
			infrastructure = 6
			11372 = {
				naval_base = 1

			}

		}
		add_core_of = GER

	}

	provinces={
		9252 9387 11260 11288 11343 
	}
	manpower=724417
	buildings_max_level_factor=1.000
	state_category=town
}
