
state={
	id=103
	name="STATE_103"

	history={
		owner = AUS
		1918.11.11 = {
			owner = SCS
			controller = SCS
			add_core_of = SCS
			remove_core_of = AUS

		}
		victory_points = {
			3924 1 
		}
		buildings = {
			infrastructure = 5
			dockyard = 1
			industrial_complex = 3
			3924 = {
				naval_base = 3

			}
			6889 = {
				naval_base = 1

			}

		}
		add_core_of = AUS

	}

	provinces={
		984 3868 3924 
	}
	manpower=1431700
	buildings_max_level_factor=1.000
	state_category=large_town
}
