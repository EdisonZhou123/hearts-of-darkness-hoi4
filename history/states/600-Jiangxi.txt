
state={
	id=600
	name="STATE_600"

	history={
		1900.1.1 = {
			owner = QIN
			add_core_of = QIN
		}
		1914.1.1 = {
			owner = ROC
			add_core_of = ROC
			remove_core_of = QIN
		}
		1918.11.11 = {
			owner = ZHI
			add_core_of = ZHI
		}
		buildings = {
			infrastructure = 3
			industrial_complex = 2

		}
		victory_points = {
			3992 3 
		}

	}

	provinces={
		1066 1110 1628 3992 4038 4083 4156 4185 7054 7172 7199 7653 9984 10057 10114 10501 11961 11988 12030 12495 13141 
	}
	manpower=15000400
	buildings_max_level_factor=1.000
	state_category=town
}
