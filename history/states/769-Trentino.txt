
state={
	id=769
	name="STATE_769"

	history={
		owner = AUS
		1918.11.11 = {
			owner = ITA
			controller = ITA
			remove_core_of = AUS

		}
		buildings = {
			infrastructure = 4
			industrial_complex = 2

		}
		add_core_of = ITA

	}

	provinces={
		6631 9598 11598 
	}
	manpower=348400
	buildings_max_level_factor=1.000
	state_category=town
}
