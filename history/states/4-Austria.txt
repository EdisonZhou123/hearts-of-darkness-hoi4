
state={
	id=4
	name="STATE_4"
	resources={
		oil=2.000
	}

	history={
		owner = AUS
		victory_points = {
			11666 30.0 
		}
		victory_points = {
			9648 5 
		}
		buildings = {
			infrastructure = 7
			arms_factory = 3
			industrial_complex = 3
			air_base = 6

		}
		add_core_of = AUS
		1938.3.12 = {
			owner = GER
			controller = GER
			add_core_of = GER

		}
		1939.1.1 = {
			buildings = {
				anti_air_building = 5

			}

		}

	}

	provinces={
		704 732 3684 3703 3718 6552 6708 6723 6739 9527 9665 11651 11666 13205 
	}
	manpower=3682000
	buildings_max_level_factor=1.000
	state_category=large_city
}
