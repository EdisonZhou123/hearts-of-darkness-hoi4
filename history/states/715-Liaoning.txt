
state={
	id=715
	name="STATE_715"

	history={
		1900.1.1 = {
			owner = QIN
			add_core_of = QIN
		}
		1914.1.1 = {
			owner = ROC
			remove_core_of = QIN
			add_core_of = ROC
		}
		1918.1.1 = {
			owner = FNT
			add_core_of = FNT
		}
		buildings = {
			infrastructure = 3
			arms_factory = 1
			industrial_complex = 1

		}

	}

	provinces={
		887 916 930 1499 1502 1558 1627 1638 3877 3913 4503 4561 4633 4642 7812 9831 11836 11903 12337 12469 12485 
	}
	manpower=3643373
	buildings_max_level_factor=1.000
	state_category=town
}
