
state={
	id=10
	name="STATE_10"

	history={
		owner = RUS
		add_core_of = RUS
		1918.11.11 = {
			owner = POL
			controller = POL
			add_core_of = POL
			remove_core_of = RUS

		}
		victory_points = {
			3544 20 
		}
		buildings = {
			infrastructure = 7
			arms_factory = 6
			industrial_complex = 2
			anti_air_building = 2
			air_base = 5

		}

	}

	provinces={
		402 467 493 524 548 562 3458 3482 3497 3521 3544 3559 3586 6416 6511 9400 9508 9521 9544 11274 11385 11428 11430 11492 11532 12562 
	}
	manpower=3089000
	buildings_max_level_factor=1.000
	state_category=large_city
}
