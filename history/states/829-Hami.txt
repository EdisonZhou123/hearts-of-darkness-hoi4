state={
	id=829
	name="STATE_829"
	
	history={
		1900.1.1 = {
			owner = QIN
			add_core_of = QIN
		}
		1914.1.1 = {
			owner = ROC
			remove_core_of = QIN
			add_core_of = ROC
		}
		1918.1.1 = {
			owner = SIK
			add_core_of = SIK
		}
		buildings = {
			infrastructure = 2
		}
		victory_points = {
			1838 1 
		}
	}	
	
	provinces={
		1838 
	}
	manpower=110000
	buildings_max_level_factor=1.000
	state_category=rural
}
