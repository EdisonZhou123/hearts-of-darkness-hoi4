
state={
	id=234
	name="STATE_234"
	resources={
		tungsten=17.000
		oil=12.000
	}

	history={
		owner = RUS
		1918.11.11 = {
			owner = VLA
			controller = VLA
			add_core_of = VLA
			remove_core_of = RUS

		}
		add_core_of = RUS
		victory_points = {
			3720 1 
		}
		victory_points = {
			3734 5 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			719 = {
				naval_base = 3
				coastal_bunker = 1

			}

		}

	}

	provinces={
		717 719 735 750 765 3587 3698 3717 3720 3734 3752 3760 3765 6692 6721 6736 6738 6741 6755 6768 6781 6783 9674 9691 9694 9696 9709 9726 9744 11668 11681 11696 11729 
	}
	manpower=2903371
	buildings_max_level_factor=1.000
	state_category=city
}
