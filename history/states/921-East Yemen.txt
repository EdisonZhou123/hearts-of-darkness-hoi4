state={
	id=921
	name="STATE_921"
	manpower = 1000500
	
	state_category = rural
	
	resources={
		oil=1
	}
	history={
		owner = YEM
		buildings = {
			infrastructure = 1
			arms_factory = 1
		}
		add_core_of = YEM
	}

	provinces={
		4924
	}
}