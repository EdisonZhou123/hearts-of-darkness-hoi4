
state={
	id=928
	name="STATE_928" # Hulialaipole
	manpower = 19198 
	state_category = town

	history={
		owner = RUS
		victory_points = {
			429 1
		}
		buildings = {
			infrastructure = 1
		}
		add_core_of = RUS
		1918.11.11 = {
			owner = FTT
			add_core_of = FTT
			add_core_of = UKR
			remove_core_of = RUS
		}
	}

	provinces={
		429
	}
}
