state= {
	id=593
	name="STATE_593"
	manpower = 12700000
	state_category = town
	resources={
		tungsten=15 # was: 20
		steel= 15 # was: 20
		chromium = 3 # was: 4
	}

	history= {
		1900.1.1 = {
			owner = QIN
			add_core_of = QIN
		}
		1914.1.1 = {
			owner = ROC
			add_core_of = ROC
			remove_core_of = QIN
		}
		1918.1.1 = {
			owner = GND
			add_core_of = GND
		}
		victory_points = {
			4050 1
		}	
		buildings = {
			infrastructure = 2
			industrial_complex = 1
			9938 = {
				naval_base = 1
				coastal_bunker = 1
				bunker = 1
			}
		}
	}
	provinces={
		1078 1120 1162 1202 4050 4165 4207 7067 7108 7141 7182 9938 9970 9978 9997 10080 12014 12095 
	}
}
