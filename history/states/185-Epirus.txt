
state={
	id=185
	name="STATE_185"
	resources={
		chromium=20.000
		steel=20.000
	}

	history={
		owner = OTT
		1914.1.1 = {
			owner = GRE

		}
		buildings = {
			infrastructure = 4
			air_base = 3
			9805 = {
				naval_base = 1

			}

		}
		add_core_of = OTT
		add_core_of = GRE

	}

	provinces={
		841 3914 3980 9805 9916 
	}
	manpower=760900
	buildings_max_level_factor=1.000
	state_category=town
}
