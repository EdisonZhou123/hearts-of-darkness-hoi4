state={
	id=890
	name="STATE_890"
	
	history={
		1900.1.1 = {
			owner = QIN
			add_core_of = QIN
		}
		1914.1.1 = {
			owner = ROC
			remove_core_of = QIN
			add_core_of = ROC
		}
		1918.1.1 = {
			owner = SIC
			add_core_of = SIC
		}
		buildings = {
			infrastructure = 3
		}

	}	
	provinces={
		1395 7240 11972 12141 12705 12819 
	}
	manpower=690000
	buildings_max_level_factor=1.000
	state_category=city
}
