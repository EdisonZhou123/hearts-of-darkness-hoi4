
state={
	id=605
	name="STATE_605"
	resources={
		steel=12.000
	}

	history={
		1900.1.1 = {
			owner = QIN
			add_core_of = QIN
		}
		1914.1.1 = {
			owner = ROC
			remove_core_of = QIN
			add_core_of = ROC
		}
		1918.1.1 = {
			owner = SIC
			add_core_of = SIC
		}
		buildings = {
			infrastructure = 4
			arms_factory = 2
			industrial_complex = 2

		}

		victory_points = {
			6999 20 
		}

	}

	provinces={
		1893 2030 2091 4041 4141 4431 5048 6999 7256 7948 10144 10304 10822 11865 12274 
	}
	manpower=52963300
	buildings_max_level_factor=1.000
	state_category=city
}
