state={
	id=772
	name="STATE_772"
	provinces={
		614 3614 6643 11787
	}
	history={
		owner = AUS
		add_core_of = SER
		1918.11.11 = {
			owner = SER
			controller = SER
		}
		buildings = {
			infrastructure = 4
			arms_factory = 1
			industrial_complex = 1

		}

	}
	manpower=597952
	buildings_max_level_factor=1.000
	state_category = large_town
}
