
state={
	id=91
	name="STATE_91"

	history={
		owner = AUS
		1918.11.11 = {
			owner = WUK
			controller = WUK
			add_core_of = WUK
			remove_core_of = AUS

		}
		victory_points = {
			11479 5 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 3
			air_base = 5

		}
		add_core_of = AUS

	}

	provinces={
		438 491 536 3434 3483 3562 3743 6431 6460 6483 9425 9454 9468 9494 9558 11411 11427 11479 11550 
	}
	manpower=3227800
	buildings_max_level_factor=1.000
	state_category=city
}
