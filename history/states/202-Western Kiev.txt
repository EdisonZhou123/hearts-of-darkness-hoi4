
state={
	id=202
	name="STATE_202"
	resources={
		oil=8.000
		aluminium=7.000
	}

	history={
		owner = RUS
		victory_points = {
			525 25 
		}
		buildings = {
			infrastructure = 7
			arms_factory = 3
			industrial_complex = 2
			air_base = 8

		}
		add_core_of = RUS
		1918.11.11 = {
			owner = UKR
			controller = UKR
			add_core_of = UKR
			remove_core_of = RUS

		}

	}

	provinces={
		489 504 525 582 3480 3494 3543 3568 6458 6497 6532 9465 9543 9568 11557 
	}
	manpower=2495352
	buildings_max_level_factor=1.000
	state_category=city
}
