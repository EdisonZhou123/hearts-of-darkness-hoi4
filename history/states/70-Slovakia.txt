
state={
	id=70
	name="STATE_70"
	resources={
		tungsten=1.000
	}

	history={
		owner = AUS
		add_core_of = AUS
		1918.11.11 = {
			owner = HUN
			controller = HUN
			add_core_of = HUN
			remove_core_of = AUS

		}
		victory_points = {
			9692 10 
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 3
			arms_factory = 2
			air_base = 3

		}

	}

	provinces={
		541 555 581 3484 3537 3550 3581 6586 6604 9539 9551 9692 11522 11539 11554 13266 
	}
	manpower=1230500
	buildings_max_level_factor=1.000
	state_category=city
}
