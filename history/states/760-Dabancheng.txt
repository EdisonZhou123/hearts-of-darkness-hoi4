
state={
	id=760
	name="STATE_760"

	history={
		1900.1.1 = {
			owner = QIN
			add_core_of = QIN
		}
		1914.1.1 = {
			owner = ROC
			remove_core_of = QIN
			add_core_of = ROC
		}
		1918.1.1 = {
			owner = SIK
			add_core_of = SIK
		}
		
		buildings = {
			infrastructure = 3
			arms_factory = 1

		}

	}

	provinces={
		4704 4843 7732 10839 12524 12598 12671 
	}
	manpower=600000
	buildings_max_level_factor=1.000
	state_category=rural
}
