﻿#########################################################################
# Kuomintang - 1908
#########################################################################
capital = 613 # Nanjing
oob = "GEN_GARRISON" # "CHI_1908"
set_convoys = 40
set_stability = 0.2
set_war_support = 0.8
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1910.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
add_ideas = {

# Cabinet

# Military Staff

}
save_global_event_target_as = WTT_current_china_leader
#######################
# Diplomacy
#######################

add_opinion_modifier = { target = PRC modifier = hostile_status }
#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "Chen Jiongming"
	desc = "Chen_Jiongming_desc"
	picture = "P_A_Chen_Jiongming.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = { }
}
# Democracy
create_country_leader = {
	name = "Sun Yat-sen"
	desc = ""
	picture = "P_D_Sun_Yat-sen.tga"
	expire = "1925.1.1"
	ideology = social_liberalism
	traits = { }
}
# Left-Wing Radicalism
create_country_leader = {
	name = "Huang Qixiang"
	desc = ""
	picture = "P_S_Huang_Qixiang.tga"
	expire = "1965.1.1"
	ideology = socialism
	traits = {}
}
#######################
# Generals
#######################
create_field_marshal = {
	name = "Ho Ying-chin"
	picture = "He_Yingqin.tga"
	traits = { offensive_doctrine trait_cautious harsh_leader }
	skill = 4
	attack_skill = 4
	defense_skill = 4
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Hsueh Yueh"
	picture = "M_Xue_Yue.tga"
	traits = { offensive_doctrine trait_reckless brilliant_strategist war_hero }
	skill = 4
	attack_skill = 4
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 3
}
