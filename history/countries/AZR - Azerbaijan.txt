﻿#########################################################################
# Azerbaijan - 1918
#########################################################################
capital = 229 #Baku

oob = "GEN_GARRISON" # "AZR_1918"

set_research_slots = 3

# Starting tech
set_technology = {
}

set_politics = {
	ruling_party = communist
	last_election = "1918.11.8"
	election_frequency = 48
	elections_allowed = no	
}

set_popularities = {
    fascist = 0
    authoritarian = 30
    democratic = 30
    socialist = 0
    communist = 40
}

create_country_leader = {
	name = "Mammad Amin Rasulzadeh"
	desc = ""
	picture = "P_D_Mammad_Amin_Rasulzadeh.tga"
	expire = "1953.3.1"
	ideology = social_democracy
	traits = {
		
	}
}

