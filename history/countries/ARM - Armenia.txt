﻿#########################################################################
# Armenia 1918
#########################################################################
capital = 230
oob = "GEN_GARRISON" # "AZR_1918"
set_research_slots = 3
#######################
# Research
#######################
set_technology = {
	Small_Arms_1918 = 1
}
#######################
# Politics
#######################
set_politics = {
	ruling_party = democratic
	last_election = "1918.11.8"
	election_frequency = 48
	elections_allowed = yes	
}

set_popularities = {
    fascist = 0
    authoritarian = 0
    democratic = 64
    socialist = 20
    communist = 16
}
#######################
# Leaders
#######################
create_country_leader = {
	name = "Hovhannes Kajaznuni"
	desc = ""
	picture = "P_D_Hovhannes_Kajaznuni.tga"
	expire = "1953.3.1"
	ideology = democratic_socialism
	traits = {}
}
