﻿#########################################################################
# Republic of China - 1914
#########################################################################
1908.1.1 = {
	capital = 620 # Wuhan
	oob = "GEN_GARRISON" # "ROC_1908"
	set_convoys = 0
	set_stability = 0.8
	set_war_support = 0.8
	#######################
	# Research
	#######################
	set_technology = {
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1911.1.1"
		election_frequency = 48
		elections_allowed = no
}

    set_popularities = {
        fascist = 0
        authoritarian = 100
        democratic = 0
        socialist = 0
        communist = 0
    }
	add_ideas = {
	# Cabinet

	# Military Staff

	}

	#######################
	# Diplomacy
	#######################
	
	#######################
	# Leaders
	#######################
	# Paternal Autocracy
	create_country_leader = {
		name = "Li Yuanhong"
		desc = "Li_Yuanhong_desc"
		picture = "P_A_Li_Yuanhong.tga"
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = { }
	}
}
#########################################################################
# Republic of China - 1914
#########################################################################
1914.1.1 = {
	capital = 608 # Beijing
	oob = "GEN_GARRISON" # "ROC_1914"
	set_convoys = 40
	set_stability = 0.2
	set_war_support = 0.5
	#######################
	# Research
	#######################
	set_technology = {
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
}

    set_popularities = {
        fascist = 0
        authoritarian = 60
        democratic = 40
        socialist = 0
        communist = 0
    }
	add_ideas = {
		
	# Cabinet

	# Military Staff

	}

	#######################
	# Diplomacy
	#######################
	
	#######################
	# Leaders
	#######################
	# Paternal Autocracy
	create_country_leader = {
		name = "Yuan Shikai"
		desc = "Yuan_Shikai_desc"
		picture = "P_A_Yuan_Shikai.tga"
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = { }
	}
}
#######################
# Generals
#######################
1918.1.1 = {
	capital = 608 # Beijing
	oob = "GEN_GARRISON" # "ROC_1914"
	set_convoys = 40
	set_stability = 0.2
	set_war_support = 0.5
	#######################
	# Research
	#######################
	set_technology = {
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
}

    set_popularities = {
        fascist = 0
        authoritarian = 60
        democratic = 40
        socialist = 0
        communist = 0
    }
	add_ideas = {
		
	# Cabinet

	# Military Staff

	}

	#######################
	# Diplomacy
	#######################
	
	#######################
	# Leaders
	#######################
	# Paternal Autocracy
	create_country_leader = {
		name = "Xu Sinchang"
		desc = "Xu_Sinchang_desc"
		picture = "P_A_Xu_Shichang.tga"
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = { }
	}
}
#######################
# Generals
#######################
