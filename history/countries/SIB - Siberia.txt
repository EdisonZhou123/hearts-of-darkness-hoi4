﻿#########################################################################
# Siberia - 1908
#########################################################################
capital = 644
set_stability = 0.7
set_war_support = 0.6
oob = "GEN_GARRISON" # "SIB_1908"
set_research_slots = 2
#######################
# Diplomacy
#######################
add_ideas = {
# National Spirits

# Laws & Policies

}
#######################
# Research
#######################
set_technology = {
}	
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1910.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
####################################################
# Leaders
####################################################
# Autocracy
create_country_leader = {
	name = "Alexander Kolchak"
		desc = ""
		picture = "gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"	#NO PORTRAIT YET
		expire = "1965.1.1"
		ideology = military_dictatorship
		traits = {}
	}