﻿capital = 231 #Tbilisi

oob = "GEN_GARRISON" # "GEO_1908"

set_research_slots = 3

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	Small_Arms_1936 = 1
	tech_recon = 1
	tech_support = 1		
	tech_engineers = 1
	tech_military_police = 1
	tech_mountaineers = 1
	truck_1936 = 1
	paratroopers = 1
	gw_artillery = 1
	
	  # PLACEHOLDER
	#  # PLACEHOLDER
	# # PLACEHOLDER
	Fighter_1933 = 1
	Fighter_1936 = 1
	Tactical_Bomber_1933 = 1
	Strategic_Bomber_1933 = 1	
	Strategic_Bomber_1936 = 1
	Naval_Bomber_1936 = 1

	transport = 1
	Mass_Assault = 1
	fleet_in_being = 1
}

set_politics = {
	ruling_party = communist
	last_election = "1932.11.8"
	election_frequency = 48
	elections_allowed = no	
}

set_popularities = {
    fascist = 0
    fascist = 0
    authoritarian = 0
    democratic = 25
    democratic = 0
    democratic = 0
    socialist = 0
    communist = 0
    communist = 75
}

create_country_leader = {
	name = "Kote Khimshiashvili"
	desc = ""
	picture = "Georgia_PA_Kote_Khimshiashvili.tga"
	expire = "1953.3.1"
	ideology = military_dictatorship
	traits = {
		
	}
}

create_country_leader = {
	name = "Noe Zhordania"
	desc = ""
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
	expire = "1953.3.1"
	ideology = social_democracy
	traits = {
		
	}
}

create_country_leader = {
	name = "Kandid Charkviani"
	desc = ""
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_land_1.dds"
	expire = "1998.3.1"
	ideology = marxism_leninism
	traits = {
		
	}
}

#couldnt find anything else, the commies simply forced everyone else into exile



