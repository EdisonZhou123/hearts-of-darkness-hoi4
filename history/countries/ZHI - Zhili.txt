﻿#########################################################################
# Zhili Clique - 1918
#########################################################################
capital = 609 
oob = "GEN_GARRISON" # "ZHI_1918"
set_convoys = 50
set_stability = 0.3
set_war_support = 0.4
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1910.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
add_ideas = {

# Cabinet

# Military Staff

}
#######################
# Diplomacy
#######################

#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "Feng Guozhang"
	desc = "Feng_Guozhang_desc"
	picture = "P_A_Feng_Guozhang.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = { }
}
#######################
# Generals
#######################
