﻿#########################################################################
# Manchucko
#########################################################################
	capital = 328
	oob = "GEN_GARRISON" # "GEN_GARRISON"
	set_convoys = 5
	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1918 = 1
		tech_support = 1		
		tech_recon = 1
		tech_engineers = 1
		marines  = 1
		gw_artillery = 1
		# Armour Tech
		Tank_1916 = 1
		Tank_1918 = 1
		Tank_1926 = 1
	}

	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = fascist
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
}

    set_popularities = {
        fascist = 38
        authoritarian = 29
        democratic = 0
        socialist = 0
        communist = 23
    }

	add_ideas = {
		# National Spirits 
		
		# Cabinet
			MAN_HoG_Chang_Chinghui
	}
