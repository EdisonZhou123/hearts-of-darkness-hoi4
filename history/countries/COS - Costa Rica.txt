﻿capital = 316

oob = "GEN_GARRISON" # "COS_1908"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
}
set_country_flag = monroe_doctrine

set_convoys = 10

set_politics = {	
	ruling_party = democratic
	last_election = "1932.2.14"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
    fascist = 0
    authoritarian = 35
    democratic = 55
    socialist = 5
    communist = 5
}
####################################################
# Costa Rican Leaders
####################################################
# democraticism
create_country_leader = {
	name = "Romualdo Jímenez Oreamuno"
	desc = "POLITICS_RICARDO_JIMENEZ_OREAMUNO_DESC"
	picture = "P_L_Romualdo_Oreamuno.tga"
	expire = "1965.1.1"
	ideology = social_liberalism
	traits = {}
}