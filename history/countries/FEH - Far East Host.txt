﻿#########################################################################
# Far East Host 
#########################################################################
capital = 574
set_stability = 0.7
set_war_support = 0.6
oob = "GEN_GARRISON" # "FEH_1908"
set_research_slots = 2
#######################
# Diplomacy
#######################
add_ideas = {
# National Spirits

# Laws & Policies

}
#######################
# Research
#######################
set_technology = {
}	
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1910.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
####################################################
# Leaders
####################################################
# Autocracy
create_country_leader = {
	name = "Grigory Mikhaylovich Semyonov"
		desc = ""
		picture = "Grigory_Mikhaylovich_Semyonov.tga"
		expire = "1965.1.1"
		ideology = military_dictatorship
		traits = {}
	}
