﻿#########################################################################
# Empire of Japan - 1908
#########################################################################
1908.1.1 = {
capital = 282
set_stability = 0.6
set_war_support = 0.7
oob = "GEN_GARRISON" # "JAP_1908"
set_research_slots = 4
set_convoys = 400
#######################
# Diplomacy
#######################
add_ideas = {
# National Spirits
	state_shintoism
# Laws & Policies
	limited_exports
	two_year_service
	partial_economic_mobilisation
# Cabinet
# Military Staff
}
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1867.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "Yoshihito"
	desc = "Yoshihito_desc"
	picture = "P_A_Meiji.png"	#NO PORTRAIT YET
	expire = "1965.1.1"
	ideology = monarchism
	traits = {}
}
create_country_leader = {
	name = "Mutsuhito"
	desc = "Mutsuhito_desc"
	picture = "P_A_Meiji.png"
	expire = "1965.1.1"
	ideology = monarchism
	traits = {}
}
#######################
# Generals
#######################
create_corps_commander = {
	name = "Tomoyuki Yamashita"
	picture = "M_Tomoyuki_Yamashita.tga"
	traits = { trickster trait_engineer brilliant_strategist politically_connected }
	skill = 5
	attack_skill = 4
	defense_skill = 5
	planning_skill = 3
	logistics_skill = 4
}

	create_field_marshal = {
	name = "Hisaichi Terauchi"
	picture = "M_Hisiachi_Terauchi.tga"
	traits = { offensive_doctrine JAP_samurai_lineage politically_connected }	#hakushaku
	skill = 4
	attack_skill = 4
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 2
}

	create_field_marshal = {
	name = "Shunroku Hata"
	picture = "M_Shunroku_Hata.tga"
	traits = { offensive_doctrine JAP_samurai_lineage }
	skill = 4
	attack_skill = 3
	defense_skill = 4
	planning_skill = 2
	logistics_skill = 4
}

create_corps_commander = {
	name = "Akira Muto"
	picture = "M_Akira_Muto.tga"
	traits = { career_officer  }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 1
}

create_corps_commander = {
	name = "Kanji Ishiwara"
	picture = "M_Kanji_Ishiwara.tga"
	traits = { JAP_samurai_lineage }
	skill = 1 
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_corps_commander = {
	name = "Shizuichi Tanaka"
	picture = "M_Shizuichi_Tanaka.tga"
	traits = { trickster career_officer infantry_officer }
	skill = 4
	attack_skill = 4
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 4
}

create_corps_commander = {
	name = "Yasuji Okamura"
	picture = "M_Yasuji_Okamura.tga"
	traits = { trait_reckless }
	skill = 1
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_corps_commander = {
	name = "Toshizo Nishio" #Toshizo
	picture = "M_Toshizo_Nishio.tga"
	traits = { brilliant_strategist infantry_officer }
	skill = 3
	attack_skill = 3
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 2
}

create_corps_commander = {
	name = "Rikichi Ando" #Ando
	picture = "M_Ando_Rikichi.tga"
	traits = { career_officer }
	skill = 3
	attack_skill = 1
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Naruhiko Higashikuni"
	picture = "M_Naruhiko_Higashikuni.tga"
	traits = { JAP_samurai_lineage politically_connected }
	skill = 3 
	attack_skill = 3
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Seishiro Itagaki"
	picture = "M_Seishiro_Itagaki.tga"
	traits = { JAP_samurai_lineage inflexible_strategist trait_reckless }
	skill = 3
	attack_skill = 3
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Kenji Doihara"
	picture = "M_Kenji_Doihara.tga"
	traits = { career_officer substance_abuser }
	skill = 1 
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_corps_commander = {
	name = "Kenkichi Ueda"
	picture = "M_Kenkichi_Ueda.tga"
	traits = { old_guard cavalry_officer trait_reckless }
	skill = 4
	attack_skill = 3
	defense_skill = 2
	planning_skill = 4
	logistics_skill = 4
}

create_corps_commander = {
	name = "M_Shigeru Honjo"
	picture = "Shigeru_Honjo.tga"
	traits = { old_guard war_hero }
	skill = 1
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_corps_commander = {
	name = "Yoshijiro Umezu"
	picture = "M_Yoshijiro_Umezu.tga"
	traits = { trait_cautious politically_connected }
	skill = 3
	attack_skill = 3
	defense_skill = 3
	planning_skill = 1
	logistics_skill = 3
}

create_corps_commander = {
	name = "Otozo Yamada"
	picture = "M_Otozo_Yamada.tga"
	traits = { cavalry_officer career_officer }
	skill = 3
	attack_skill = 3
	defense_skill = 1
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Hatazo Adachi"
	picture = "M_Hatazo_Adachi.tga"
	traits = { trickster JAP_samurai_lineage trait_reckless infantry_officer }
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Iwane Matsui"
	picture = "M_Iwane_Matsui.tga"
	traits = { old_guard trait_reckless }
	skill = 1
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}
create_corps_commander = {
	name = "Sadao Araki"
	picture = "Japan_F_Sadao_Araki.tga"
	traits = { JAP_samurai_lineage }
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Keisuke Fujie"
	picture = "M_Keisuke_Fujie.tga"
	traits = { fortress_buster career_officer politically_connected } 
	skill = 3
	attack_skill = 1
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 3
}
create_corps_commander = {
	name = "Kiichiro Higuchi"
	picture = "M_Kiichiro_Higuchi.tga"
	traits = { career_officer trait_cautious }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 1
}

create_corps_commander = {
	name = "Masaharu Homma"
	picture = "M_Masaharu_Homma.tga"
	traits = { media_personality trait_cautious }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 1
	logistics_skill = 2
}

create_corps_commander = {
	name = "Harukichi Hyakutake"
	picture = "M_Harukichi_Hyakutake.tga"
	traits = { infantry_officer  }
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Jo Iimura"
	picture = "Jo_Iimura.tga"
	traits = { career_officer }
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Hitoshi Imamura"
	picture = "Hitoshi_Imamura.tga"
	traits = { commando career_officer infantry_officer }
	skill = 3
	attack_skill = 3
	defense_skill = 1
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Masatane Kanda"
	picture = "Masatane_Kanda.tga"
	traits = { trait_cautious }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 1
	logistics_skill = 2
}

create_corps_commander = {
	name = "Kuniaki Koiso"
	picture = "P_A_Kuniaki_Koiso.tga"
	traits = { JAP_samurai_lineage old_guard politically_connected }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 1
}
	#######################
	# Admirals
	#######################
create_navy_leader = {
	name = "Isoroku Yamamoto"
	picture = "M_Isoroku_Yamamoto.tga"
	traits = { superior_tactician spotter }
	skill = 5
}

create_navy_leader = {
	name = "Mineichi Koga"
	picture = "M_Mineichi_Koga.tga"
	traits = { superior_tactician spotter }
	skill = 4
}

create_navy_leader = {
	name = "Kiyoshi Hasegawa"
	picture = "M_Hasegawa_Kiyoshi.tga"
	traits = {  }
	skill = 2
}

	# Keen on planes
create_navy_leader = {
	name = "Shigeyoshi Inoue"
	picture = "M_Shigeyoshi_Inoue.tga"
	traits = { air_controller }
	skill = 1
}

create_navy_leader = {
	name = "Nobutake Kondo"
	picture = "M_Nobutake_Kondo.tga"
	traits = { fly_swatter }
	skill = 3
}

create_navy_leader = {
	name = "Takeo Takagi"
	picture = "Takeo_Takagi.tga"
	traits = {  }
	skill = 2
}

create_navy_leader = {
	name = "Soemu Toyoda"
	picture = "Soemu_Toyoda.tga"
	traits = { old_guard_navy }
	skill = 1
}

create_navy_leader = {
	name = "Jisaburo Ozawa"
	picture = "Jisaburo_Ozawa.tga"
	traits = { blockade_runner superior_tactician }
	skill = 5
	}

create_navy_leader = {
	name = "Zengo Yoshida"
	picture = "Zengo_Yoshida.tga"
	traits = {  }
	skill = 2
}

create_navy_leader = {
	name = "Hiroaki Abe"
	picture = "Hiroaki_Abe.tga"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Tadashige Daigo"
	picture = "Tadashige_Daigo.tga"
	traits = { seawolf }
	skill = 1
}

create_navy_leader = {
	name = "Gengo Hyakutake"
		picture = "Gengo_Hyakutake.tga"
		traits = {  }
		skill = 3
	}
}
#########################################################################
# Empire of Japan - 1914
#########################################################################
1914.1.1 = {
capital = 282
set_stability = 0.6
set_war_support = 0.7
oob = "GEN_GARRISON" # "JAP_1914"
set_research_slots = 4
set_convoys = 400
#######################
# Diplomacy
#######################
add_ideas = {
# National Spirits
	state_shintoism
# Laws & Policies
	limited_exports
	two_year_service
	partial_economic_mobilisation
# Cabinet
# Military Staff
}
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1912.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "Yoshihito"
	desc = "Yoshihito_desc"
	picture = "P_A_Meiji.png"	#NO PORTRAIT YET
	expire = "1965.1.1"
	ideology = monarchism
	traits = {}
}
#######################
# Generals
#######################
create_corps_commander = {
	name = "Tomoyuki Yamashita"
	picture = "M_Tomoyuki_Yamashita.tga"
	traits = { trickster trait_engineer brilliant_strategist politically_connected }
	skill = 5
	attack_skill = 4
	defense_skill = 5
	planning_skill = 3
	logistics_skill = 4
}

	create_field_marshal = {
	name = "Hisaichi Terauchi"
	picture = "M_Hisiachi_Terauchi.tga"
	traits = { offensive_doctrine JAP_samurai_lineage politically_connected }	#hakushaku
	skill = 4
	attack_skill = 4
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 2
}

	create_field_marshal = {
	name = "Shunroku Hata"
	picture = "M_Shunroku_Hata.tga"
	traits = { offensive_doctrine JAP_samurai_lineage }
	skill = 4
	attack_skill = 3
	defense_skill = 4
	planning_skill = 2
	logistics_skill = 4
}

create_corps_commander = {
	name = "Akira Muto"
	picture = "M_Akira_Muto.tga"
	traits = { career_officer  }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 1
}

create_corps_commander = {
	name = "Kanji Ishiwara"
	picture = "M_Kanji_Ishiwara.tga"
	traits = { JAP_samurai_lineage }
	skill = 1 
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_corps_commander = {
	name = "Shizuichi Tanaka"
	picture = "M_Shizuichi_Tanaka.tga"
	traits = { trickster career_officer infantry_officer }
	skill = 4
	attack_skill = 4
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 4
}

create_corps_commander = {
	name = "Yasuji Okamura"
	picture = "M_Yasuji_Okamura.tga"
	traits = { trait_reckless }
	skill = 1
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_corps_commander = {
	name = "Toshizo Nishio" #Toshizo
	picture = "M_Toshizo_Nishio.tga"
	traits = { brilliant_strategist infantry_officer }
	skill = 3
	attack_skill = 3
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 2
}

create_corps_commander = {
	name = "Rikichi Ando" #Ando
	picture = "M_Ando_Rikichi.tga"
	traits = { career_officer }
	skill = 3
	attack_skill = 1
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Naruhiko Higashikuni"
	picture = "M_Naruhiko_Higashikuni.tga"
	traits = { JAP_samurai_lineage politically_connected }
	skill = 3 
	attack_skill = 3
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Seishiro Itagaki"
	picture = "M_Seishiro_Itagaki.tga"
	traits = { JAP_samurai_lineage inflexible_strategist trait_reckless }
	skill = 3
	attack_skill = 3
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Kenji Doihara"
	picture = "M_Kenji_Doihara.tga"
	traits = { career_officer substance_abuser }
	skill = 1 
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_corps_commander = {
	name = "Kenkichi Ueda"
	picture = "M_Kenkichi_Ueda.tga"
	traits = { old_guard cavalry_officer trait_reckless }
	skill = 4
	attack_skill = 3
	defense_skill = 2
	planning_skill = 4
	logistics_skill = 4
}

create_corps_commander = {
	name = "M_Shigeru Honjo"
	picture = "Shigeru_Honjo.tga"
	traits = { old_guard war_hero }
	skill = 1
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_corps_commander = {
	name = "Yoshijiro Umezu"
	picture = "M_Yoshijiro_Umezu.tga"
	traits = { trait_cautious politically_connected }
	skill = 3
	attack_skill = 3
	defense_skill = 3
	planning_skill = 1
	logistics_skill = 3
}

create_corps_commander = {
	name = "Otozo Yamada"
	picture = "M_Otozo_Yamada.tga"
	traits = { cavalry_officer career_officer }
	skill = 3
	attack_skill = 3
	defense_skill = 1
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Hatazo Adachi"
	picture = "M_Hatazo_Adachi.tga"
	traits = { trickster JAP_samurai_lineage trait_reckless infantry_officer }
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Iwane Matsui"
	picture = "M_Iwane_Matsui.tga"
	traits = { old_guard trait_reckless }
	skill = 1
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}
create_corps_commander = {
	name = "Sadao Araki"
	picture = "Japan_F_Sadao_Araki.tga"
	traits = { JAP_samurai_lineage }
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Keisuke Fujie"
	picture = "M_Keisuke_Fujie.tga"
	traits = { fortress_buster career_officer politically_connected } 
	skill = 3
	attack_skill = 1
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 3
}
create_corps_commander = {
	name = "Kiichiro Higuchi"
	picture = "M_Kiichiro_Higuchi.tga"
	traits = { career_officer trait_cautious }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 1
}

create_corps_commander = {
	name = "Masaharu Homma"
	picture = "M_Masaharu_Homma.tga"
	traits = { media_personality trait_cautious }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 1
	logistics_skill = 2
}

create_corps_commander = {
	name = "Harukichi Hyakutake"
	picture = "M_Harukichi_Hyakutake.tga"
	traits = { infantry_officer  }
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Jo Iimura"
	picture = "Jo_Iimura.tga"
	traits = { career_officer }
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Hitoshi Imamura"
	picture = "Hitoshi_Imamura.tga"
	traits = { commando career_officer infantry_officer }
	skill = 3
	attack_skill = 3
	defense_skill = 1
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Masatane Kanda"
	picture = "Masatane_Kanda.tga"
	traits = { trait_cautious }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 1
	logistics_skill = 2
}

create_corps_commander = {
	name = "Kuniaki Koiso"
	picture = "P_A_Kuniaki_Koiso.tga"
	traits = { JAP_samurai_lineage old_guard politically_connected }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 1
}
	#######################
	# Admirals
	#######################
create_navy_leader = {
	name = "Isoroku Yamamoto"
	picture = "M_Isoroku_Yamamoto.tga"
	traits = { superior_tactician spotter }
	skill = 5
}

create_navy_leader = {
	name = "Mineichi Koga"
	picture = "M_Mineichi_Koga.tga"
	traits = { superior_tactician spotter }
	skill = 4
}

create_navy_leader = {
	name = "Kiyoshi Hasegawa"
	picture = "M_Hasegawa_Kiyoshi.tga"
	traits = {  }
	skill = 2
}

	# Keen on planes
create_navy_leader = {
	name = "Shigeyoshi Inoue"
	picture = "M_Shigeyoshi_Inoue.tga"
	traits = { air_controller }
	skill = 1
}

create_navy_leader = {
	name = "Nobutake Kondo"
	picture = "M_Nobutake_Kondo.tga"
	traits = { fly_swatter }
	skill = 3
}

create_navy_leader = {
	name = "Takeo Takagi"
	picture = "Takeo_Takagi.tga"
	traits = {  }
	skill = 2
}

create_navy_leader = {
	name = "Soemu Toyoda"
	picture = "Soemu_Toyoda.tga"
	traits = { old_guard_navy }
	skill = 1
}

create_navy_leader = {
	name = "Jisaburo Ozawa"
	picture = "Jisaburo_Ozawa.tga"
	traits = { blockade_runner superior_tactician }
	skill = 5
	}

create_navy_leader = {
	name = "Zengo Yoshida"
	picture = "Zengo_Yoshida.tga"
	traits = {  }
	skill = 2
}

create_navy_leader = {
	name = "Hiroaki Abe"
	picture = "Hiroaki_Abe.tga"
	traits = {  }
	skill = 1
}

create_navy_leader = {
	name = "Tadashige Daigo"
	picture = "Tadashige_Daigo.tga"
	traits = { seawolf }
	skill = 1
}

create_navy_leader = {
	name = "Gengo Hyakutake"
		picture = "Gengo_Hyakutake.tga"
		traits = {  }
		skill = 3
	}
}