﻿capital = 307

oob = "GEN_GARRISON" # "VEN_1914"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	gw_artillery = 1
	Fighter_1933 = 1
	
	DD_1885 = 1
	DD_1900 = 1
	DD_1912 = 1
	DD_1916 = 1
	DD_1922 = 1
	DD_1933 = 1	
	
	SS_1895 = 1
	SS_1912 = 1
}
set_country_flag = monroe_doctrine

set_convoys = 10
set_politics = {	
	ruling_party = authoritarian
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 65
    democratic = 20
    socialist = 15
    communist = 0
}
####################################################
# Venezuelan Leaders
####################################################
# Paternal Autocracy
create_country_leader = {
	name = "Eleazar Contreras"
	desc = ""
	picture = "P_A_Elezar_Lopez_Contreras.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}