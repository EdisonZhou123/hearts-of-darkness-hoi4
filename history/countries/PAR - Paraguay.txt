﻿capital = 301

oob = "GEN_GARRISON" # "PAR_1914"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	gw_artillery = 1
	Fighter_1933 = 1
}
set_country_flag = monroe_doctrine

set_politics = {	
	ruling_party = democratic
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
    fascist = 0
    authoritarian = 0
    democratic = 90
    socialist = 10
    communist = 0
}

create_country_leader = {
	name = "Eusebio Ayala"
	desc = "POLITICS_EUSEBIO_AYALA_DESC"
	picture = "P_D_Eusebio_Ayala.dds"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {}
}