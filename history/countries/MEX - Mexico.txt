﻿#########################################################################
# Mexico - 1908
#########################################################################
1908.1.1 = {
capital = 277
oob = "GEN_GARRISON" # "GEN_GARRISON"
set_stability = 0.7
set_war_support = 0.6
set_research_slots = 2
set_convoys = 45
#######################
# Diplomacy
#######################
add_ideas = {
#Cabinet

#Ideas

}

set_country_flag = monroe_doctrine

#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1906.1.1"
	election_frequency = 72
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 56
    democratic = 40
    socialist = 4
    communist = 0
}
#######################
# Leaders
#######################
# Autocracy
create_country_leader = {
	name = "Venustiano Carranza"
	desc = ""
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"	#NO PORTRAIT YET
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}
create_country_leader = {
	name = "Porfirio Díaz"
	desc = ""
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"	#NO PORTRAIT YET
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}
#######################
# Generals
#######################
create_corps_commander = {
	name = "Luis Farell"
	portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_land_2.dds"
	traits = { }
	skill = 3
	attack_skill = 4
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Gildardo Magaña"
	portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_land_4.dds"
	traits = { desert_fox }
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}
#######################
# Admirals
#######################
create_navy_leader = {
	name = "Paul Suarez"
		portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_navy_2.dds"
		traits = { superior_tactician }
		skill = 3
	}
}
#########################################################################
# Mexico - 1914
#########################################################################
1914.1.1 = {
capital = 277
oob = "GEN_GARRISON" # "MEX_1914"
set_stability = 0.7
set_war_support = 0.6
set_research_slots = 2
set_convoys = 45
#######################
# Diplomacy
#######################
add_ideas = {
#Cabinet

#Ideas

}

set_country_flag = monroe_doctrine

#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1910.1.1"
	election_frequency = 72
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 56
    democratic = 40
    socialist = 4
    communist = 0
}
#######################
# Leaders
#######################
# Autocracy
create_country_leader = {
	name = "Venustiano Carranza"
	desc = ""
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"	#NO PORTRAIT YET
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}
#######################
# Generals
#######################
create_corps_commander = {
	name = "Luis Farell"
	portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_land_2.dds"
	traits = { }
	skill = 3
	attack_skill = 4
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Gildardo Magaña"
	portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_land_4.dds"
	traits = { desert_fox }
	skill = 3
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}
#######################
# Admirals
#######################
create_navy_leader = {
	name = "Paul Suarez"
		portrait_path = "gfx/leaders/South America/Portrait_South_America_Generic_navy_2.dds"
		traits = { superior_tactician }
		skill = 3
	}
}