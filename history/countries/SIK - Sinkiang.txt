﻿#########################################################################
# Sinkiang 1914
#########################################################################
	capital = 617
	oob = "GEN_GARRISON" # "XNJ_1914"
	#######################
	# Research
	#######################
	set_technology = {
		Small_Arms_1918 = 1
	}
	#######################
	# Politics
	#######################
	set_politics = {
		ruling_party = authoritarian
		last_election = "1936.1.1"
		election_frequency = 48
		elections_allowed = no
}

    set_popularities = {
        fascist = 0
        authoritarian = 45
        democratic = 15
        socialist = 30
        communist = 10
    }
	#######################
	# Leaders
	#######################
	# Authoritarian
	create_country_leader = {
		name = "Chin Shu-jen"
		desc = "POLITICS_JIN_SHUREN_DESC"
		picture = "P_A_Jin_Shuren.tga"
		expire = "1965.1.1"
		ideology = authoritarian_democracy
		traits = {}
	}
	#######################
	# Generals
	#######################
	create_corps_commander = {
		name = "Chin Shu-jen"
		picture = "P_A_Jin_Shuren.tga"
		traits = { }
		skill = 3
		attack_skill = 4
		defense_skill = 2
		planning_skill = 2
		logistics_skill = 2
	}
