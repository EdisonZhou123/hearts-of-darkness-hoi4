﻿capital = 313

oob = "GEN_GARRISON" # "GUA_1908"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	gw_artillery = 1
	Fighter_1933 = 1
}
set_country_flag = monroe_doctrine

set_convoys = 5


set_politics = {	
	ruling_party = authoritarian
	last_election = "1931.2.8"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 70
    democratic = 30
    socialist = 4
    communist = 0
}
####################################################
# Guatemalan Leaders
####################################################
# Paternal Autocracy
create_country_leader = {
	name = "Jorge Ubico Castaneda"
	desc = "POLITICS_JORGE_UBICO_DESC"
	picture = "P_A_Jorge_Castaneda.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}