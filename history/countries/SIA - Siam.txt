﻿capital = 289

oob = "GEN_GARRISON" # "SIA_1914"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	gw_artillery = 1
	Fighter_1933 = 1
	CAS_1936 = 1
#Naval Stuff
	DD_1885 = 1
	DD_1900 = 1
	DD_1912 = 1
	DD_1916 = 1
	
	CL_1885 = 1
	CL_1900 = 1
	CL_1912 = 1
	
	SS_1895 = 1
	SS_1912 = 1
#
}

set_convoys = 15

set_politics = {	
	ruling_party = democratic
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 30
    democratic = 70
    socialist = 0
    communist = 0
}

create_country_leader = {
	name = "Rama VIII"
	desc = "POLITICS_PHRAYA_PHAHON_DESC"
	picture = "Portrait_Siam_Rama_VIII.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}
create_country_leader = {
	name = "Phraya Phahon"
	desc = "POLITICS_PHRAYA_PHAHON_DESC"
	picture = "P_A_Phraya_Phahon.tga"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {}
}