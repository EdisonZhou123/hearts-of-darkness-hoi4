﻿#########################################################################
# Argentina
#########################################################################
capital = 278
oob = "GEN_GARRISON" # "ARG_1908"
set_convoys = 120
#######################
# Research
#######################
set_technology = {

# Armour Tech

# Naval Tech

}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = democratic
	last_election = "1931.11.8"
	election_frequency = 72
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 43
    democratic = 52
    socialist = 5
    communist = 0
}
add_ideas = {
# Cabinet

# Military Staff

}
#######################
# Diplomacy
#######################
set_country_flag = monroe_doctrine
#######################
# Leaders
#######################
# Democracy
create_country_leader = {
	name = "José Figueroa Alcorta"
	desc = ""
	picture = "P_D_Jose_Figueroa_Alcorta.tga"
	expire = "1965.1.11"
	ideology = social_liberalism
	traits = {}
}


#######################
# Generals
#######################

#######################
# Admirals
#######################
