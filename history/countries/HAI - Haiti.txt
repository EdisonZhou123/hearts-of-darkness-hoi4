﻿capital = 318

oob = "GEN_GARRISON" # "HAI_1908"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
}
set_country_flag = monroe_doctrine

set_convoys = 5

set_politics = {	
	ruling_party = democratic
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 23
    democratic = 72
    socialist = 5
    communist = 0
}
####################################################
# Haitian Leaders
####################################################
# Conservatism
create_country_leader = {
	name = "Sténio Vincent"
	desc = "POLITICS_STENIO_VINCENT_DESC"
	picture = "P_D_Stenio_Vincent.tga"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {}
}