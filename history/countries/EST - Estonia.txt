﻿capital = 13

oob = "GEN_GARRISON" # "EST_1908"

set_research_slots = 3

add_ideas = {
	two_year_service
}

set_technology = {
	Small_Arms_1918 = 1
	Small_Arms_1936 = 1
	Fighter_1933 = 1
#Naval Stuff	
	SS_1895 = 1
	SS_1912 = 1
	SS_1916 = 1
#
}

set_convoys = 5

set_politics = {	
	ruling_party = authoritarian
	last_election = "1932.5.21"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 1
    authoritarian = 65
    democratic = 23
    socialist = 5
    communist = 6
}

####################################################
# Estonian Leaders
####################################################
# Paternal Autocracy
create_country_leader = {
	name = "Konstantin Päts"
	desc = ""
	picture = "P_A_Konstantin_Pats.tga"
	expire = "1965.1.1"
	ideology = authoritarian_democracy
	traits = {}
}