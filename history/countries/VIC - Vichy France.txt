﻿capital = 26  

set_research_slots = 2

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
	Small_Arms_1936 = 1
	tech_recon = 1
	tech_support = 1
	tech_engineers = 1
	tech_mountaineers = 1
	truck_1936 = 1
	gw_artillery = 1
	interwar_antiair = 1
	
	
	
	
	Fighter_1933 = 1
	Fighter_1936 = 1
	Tactical_Bomber_1933 = 1
	Tactical_Bomber_1936 = 1
	Strategic_Bomber_1933 = 1
	Strategic_Bomber_1936 = 1
	CAS_1936 = 1
	Naval_Bomber_1936 = 1
#Naval Stuff
	DD_1885 = 1
	DD_1900 = 1
	DD_1912 = 1
	DD_1916 = 1
	DD_1922 = 1
	DD_1933 = 1	
	
	CL_1885 = 1
	CL_1900 = 1
	CL_1912 = 1
	CL_1922 = 1
	CL_1933 = 1	
	CL_1936 = 1
	
	CA_1885 = 1
	CA_1895 = 1
	CA_1906 = 1
	CA_1922 = 1
	CA_1933 = 1	
	CA_1936 = 1	
	
	SS_1895 = 1
	SS_1912 = 1
	SS_1916 = 1
	SS_1922 = 1	
	
	BC_1906 = 1
	BC_1912 = 1
	BC_1916 = 1
	BC_1933 = 1
	
	BB_1885 = 1
	BB_1895 = 1
	BB_1900 = 1
	BB_1906 = 1
	BB_1912 = 1
	BB_1916 = 1
	BB_1922 = 1	
	BB_1933 = 1
	
	CV_1912 = 1
	CV_1916 = 1
	
	CV_1922 = 1	
	
	transport = 1
#
	Mobility_Focus = 1
	trade_interdiction = 1
	formation_flying = 1
	synth_oil_experiments = 1
}

set_politics = {	
	ruling_party = authoritarian
	elections_allowed = no
}

set_popularities = {
    fascist = 8
    authoritarian = 65
    democratic = 27
    socialist = 0
    communist = 0
}

set_convoys = 25
set_stability = 0.45 



create_country_leader = {
	name = "Philippe Pétain"
	desc = "POLITICS_PHILIPPE_PÉTAIN_DESC"
	picture = "P_A_Philippe_Petain.tga"
	expire = "1965.1.1"
	ideology = military_dictatorship
	traits = { fascist_sympathies }
}

create_field_marshal = {
	name = "Maxime Weygand"
	picture = "Portrait_Vichy_Maxime_Weygand.dds"
	traits = { defensive_doctrine old_guard }
	skill = 3
}

create_navy_leader = {
	name = "François Darlan"
	picture = "Portrait_Vichy_Francois_Darlan.dds"
	traits = { superior_tactician }
	skill = 3
}

create_navy_leader = {
	name = "Jean-Marie Charles Abrial"
	picture = "Portrait_Vichy_Jean-Marie_Charles_Abrial.dds"
	traits = { superior_tactician }
	skill = 3
}

create_navy_leader = {
	name = "Jean-Pierre Esteva"
	picture = "Portrait_Vichy_Jean-Pierre_Esteva.dds"
	traits = { seawolf }
	skill = 2
}

create_navy_leader = {
	name = "René-Émile Godfroy"
	picture = "Portrait_Vichy_Rene-Emile_Godfroy.dds"
	traits = { old_guard_navy spotter }
	skill = 2
}

create_navy_leader = {
	name = "Jean de Laborde"
	picture = "Portrait_Vichy_Jean_de_Laborde.dds"
	traits = {  }
	skill = 1
}
