﻿#########################################################################
# Kingdom of Serbia - 1908
#########################################################################
1908.1.1 = {
capital = 107
set_stability = 0.7
set_war_support = 0.8
oob = "GEN_GARRISON" # "SER_1908"
set_research_slots = 2
set_convoys = 50
#######################
# Diplomacy
#######################
add_ideas = {
# National Spirits
# Cabinet
# Military Staff
}
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1903.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 88
    democratic = 0
    socialist = 8
    communist = 4
}

create_country_leader = {
	name = "Peter I"
		desc = ""
		picture = "P_A_Peter_I.tga"
		expire = "1965.1.1"
		ideology = monarchism
		traits = {
			#
		}
	}
}
#########################################################################
# Kingdom of Serbia - 1914
#########################################################################
1914.1.1 = {
capital = 107
set_stability = 0.6
set_war_support = 0.8
oob = "GEN_GARRISON" # "SER_1914"
set_research_slots = 2
set_convoys = 50
#######################
# Diplomacy
#######################
add_ideas = {
# National Spirits
anti_german_military
# Cabinet
# Military Staff
}
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1903.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 76
    democratic = 12
    socialist = 8
    communist = 4
}

create_country_leader = {
	name = "Peter I"
		desc = ""
		picture = "P_A_Peter_I.tga"
		expire = "1965.1.1"
		ideology = monarchism
		traits = {
			#
		}
	}
}