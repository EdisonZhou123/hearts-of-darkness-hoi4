﻿#########################################################################
# Greece - 1908
#########################################################################
1908.1.1 = {
capital = 47
set_stability = 0.7
set_war_support = 0.6
oob = "GEN_GARRISON" # "GRE_1908"
set_research_slots = 3
set_convoys = 10
#######################
# Diplomacy
#######################
add_ideas = {
# Laws & Policies
	two_year_service
}
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1863.1.1"
	election_frequency = 60
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 76
    democratic = 24
    socialist = 0
    communist = 0
}
#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "George I"
	desc = ""
	picture = "P_A_George_I.png"
	expire = "1965.1.1"
	ideology = monarchism
	traits = {}
}
#######################
# Generals
#######################
create_field_marshal = {
	name = "Alexandros Papagos"
	picture = "M_Alexandros_Papagos.tga"
	traits = { old_guard politically_connected }
	skill = 4 
	attack_skill = 4
	defense_skill = 3 
	planning_skill = 4
	logistics_skill = 3
}

create_corps_commander = {
	name = "Charalampos Katsimiros"
	picture = "M_Charalampos_Katsimiros.tga"
	traits = { infantry_officer hill_fighter }
	skill = 4
	attack_skill = 3
	defense_skill = 4
	planning_skill = 4
	logistics_skill = 3
}

create_corps_commander = {
	name = "Konstantinos Davakis"
	picture = "M_Konstantinos_Davakis.tga"
	traits = { brilliant_strategist cavalry_officer }
	skill = 4 
	attack_skill = 5
	defense_skill = 4
	planning_skill = 4
	logistics_skill = 3
}

create_corps_commander = {
	name = "Vasileios Vrachnos"
	picture = "M_Vasileios_Vrachnos.tga"
	traits = { infantry_officer }
	skill = 3  
	attack_skill = 2
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Georgios Bakos"
	picture = "M_Georgios_Bakos.tga"
	traits = { infantry_officer }
	skill = 2 
	attack_skill = 2
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Georgios Tsokaloglou"
	picture = "M_Georgios_Tsokaloglou.tga"
	traits = { hill_fighter }
	skill = 2 
	attack_skill = 3
	defense_skill = 2
	planning_skill = 2  
	logistics_skill = 2
}
#######################
# Admirals
#######################
create_navy_leader = {
	name = "Dimitrios Oikonomou"
	picture = "M_Dimitrios_Oikonomou.tga"
	traits = { old_guard_navy }
	skill = 3 
}

create_navy_leader = {
	name = "Alexandros Sakellariou"
		picture = "M_Alexandros_Sakellariou.tga"
		traits = { superior_tactician }
		skill = 4 
	}
}
#########################################################################
# Greece - 1914
#########################################################################
1914.1.1 = {
capital = 47
set_stability = 0.7
set_war_support = 0.6
oob = "GEN_GARRISON" # "GRE_1914"
set_research_slots = 3
set_convoys = 10
#######################
# Diplomacy
#######################
add_ideas = {
# Laws & Policies
	two_year_service
}
#######################
# Research
#######################
set_technology = {
}
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1913.1.1"
	election_frequency = 60
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 76
    democratic = 24
    socialist = 0
    communist = 0
}
#######################
# Leaders
#######################
# Paternal Autocracy
create_country_leader = {
	name = "Constantine I"
	desc = ""
	picture = "P_A_George_I.png"	#NO PORTRAIT YET
	expire = "1965.1.1"
	ideology = monarchism
	traits = {}
}
#######################
# Generals
#######################
create_field_marshal = {
	name = "Alexandros Papagos"
	picture = "M_Alexandros_Papagos.tga"
	traits = { old_guard politically_connected }
	skill = 4 
	attack_skill = 4
	defense_skill = 3 
	planning_skill = 4
	logistics_skill = 3
}

create_corps_commander = {
	name = "Charalampos Katsimiros"
	picture = "M_Charalampos_Katsimiros.tga"
	traits = { infantry_officer hill_fighter }
	skill = 4
	attack_skill = 3
	defense_skill = 4
	planning_skill = 4
	logistics_skill = 3
}

create_corps_commander = {
	name = "Konstantinos Davakis"
	picture = "M_Konstantinos_Davakis.tga"
	traits = { brilliant_strategist cavalry_officer }
	skill = 4 
	attack_skill = 5
	defense_skill = 4
	planning_skill = 4
	logistics_skill = 3
}

create_corps_commander = {
	name = "Vasileios Vrachnos"
	picture = "M_Vasileios_Vrachnos.tga"
	traits = { infantry_officer }
	skill = 3  
	attack_skill = 2
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Georgios Bakos"
	picture = "M_Georgios_Bakos.tga"
	traits = { infantry_officer }
	skill = 2 
	attack_skill = 2
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Georgios Tsokaloglou"
	picture = "M_Georgios_Tsokaloglou.tga"
	traits = { hill_fighter }
	skill = 2 
	attack_skill = 3
	defense_skill = 2
	planning_skill = 2  
	logistics_skill = 2
}
#######################
# Admirals
#######################
create_navy_leader = {
	name = "Dimitrios Oikonomou"
	picture = "M_Dimitrios_Oikonomou.tga"
	traits = { old_guard_navy }
	skill = 3 
}

create_navy_leader = {
	name = "Alexandros Sakellariou"
		picture = "M_Alexandros_Sakellariou.tga"
		traits = { superior_tactician }
		skill = 4 
	}
}