﻿capital = 304

oob = "GEN_GARRISON" # "PAN_1914"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
}
set_country_flag = monroe_doctrine

set_convoys = 10

set_politics = {	
	ruling_party = democratic
	last_election = "1932.6.5"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
    fascist = 0
    authoritarian = 0
    democratic = 100
    socialist = 0
    communist = 0
}

####################################################
# Panama Leaders
####################################################
# Conservatism
create_country_leader = {
	name = "Harmodio Arias Madrid"
	desc = "POLITICS_HARMODIO_ARIAS_MADRID_DESC"
	picture = "P_D_Harmodio_Arias_Madrid.tga"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {}
}
