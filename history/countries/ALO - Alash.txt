﻿#########################################################################
# Alash - 1908
#########################################################################
1908.1.1 = {
capital = 590
set_stability = 0.6
set_war_support = 0.7
oob = "GEN_GARRISON" # "ALO_1908"
set_research_slots = 2
set_convoys = 10
#######################
# Diplomacy
#######################
add_ideas = {
# National Spirits

# Laws & Policies

}
#######################
# Research
#######################
set_technology = {
}	
#######################
# Politics
#######################
set_politics = {	
	ruling_party = democratic
	last_election = "1906.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 25
    democratic = 60
    socialist = 0
    communist = 15
}
####################################################
# Leaders
####################################################
# Paternal Autocracy
create_country_leader = {
	name = "Alikhan Bukeikhanov"
		desc = ""
		picture = "P_D_Alikhan_Bukeikhanov.tga"	#NO PORTRAIT YET
		expire = "1965.1.1"
		ideology = social_liberalism
		traits = {}
	}
}