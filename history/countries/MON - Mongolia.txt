﻿capital = 330

oob = "GEN_GARRISON" # "GEN_GARRISON"

# Starting tech
set_technology = {
	Small_Arms_1918 = 1
}

add_ideas = {
	two_year_service
}

set_politics = {	
	ruling_party = communist
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    fascist = 0
    authoritarian = 4
    democratic = 8
    democratic = 3
    democratic = 0
    socialist = 0
    communist = 25
    communist = 60
}

####################################################
# Mongolia Leaders
####################################################
# Bolshevik-Leninism
create_country_leader = {
	name = "Agdanbuugiyn Amar"
	desc = "POLITICS_ANANDYN_AMAR_DESC"
	picture = "P_C_Agdanbuugiyn_Amar.tga"
	expire = "1965.1.1"
	ideology = marxism_leninism
	traits = {}
}