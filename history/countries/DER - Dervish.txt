﻿#########################################################################
# Dervish - 1908
#########################################################################
1908.1.1 = {
capital = 905
set_stability = 0.45
set_war_support = 0.3
set_research_slots = 2
#######################
# Diplomacy
#######################
add_ideas = {
# National Spirits

# Laws & Policies

}
#######################
# Research
#######################
set_technology = {
}	
#######################
# Politics
#######################
set_politics = {	
	ruling_party = authoritarian
	last_election = "1906.1.1"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
    fascist = 0
    authoritarian = 100
    democratic = 0
    socialist = 0
    communist = 0
}
####################################################
# Leaders
####################################################
# Autocracy
create_country_leader = {
	name = "	Mohammed Abdullah Hassan"
		desc = ""
		picture = "P_A_Mohammed_Abdullah_Hassa.tga"	
		expire = "1965.1.1"
		ideology = monarchism
		traits = {}
	}
}